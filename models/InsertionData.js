export const InsertionDataSchema = {
  type: "object",
  properties: {
    count: { type: "number" },
    docNumber: { type: "string" },
    placeCode: { type: "string" },
    productCode: { type: "string" },
    userCode: { type: "string" },
  },
  required: ["count", "docNumber", "placeCode", "productCode", "userCode"],
};

export class InsertionData {
  userCode;
  docNumber;
  placeCode;
  productCode;
  count;

  /**
   * @param {string} userCode
   * @param {string} docNumber
   * @param {string} placeCode
   * @param {string} productCode
   * @param {number} count
   */
  constructor(userCode, docNumber, placeCode, productCode, count) {
    this.userCode = userCode;
    this.docNumber = docNumber;
    this.placeCode = placeCode;
    this.productCode = productCode;
    this.count = count;
  }

  // TODO: TEST
  toDTO() {
    return {
      Document: this.docNumber,
      Place: this.placeCode,
      Barcode: this.productCode,
      Quantity: this.count,
      UserID: this.userCode,
    };
  }
}
