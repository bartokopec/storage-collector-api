export class AppError extends Error {
  constructor(message) {
    super(`[APP ERROR] ${message}`);
  }
}

export const Errors = {
  BAD_FORMAT: "badFormat",
  UNAUTHORIZED: "unauthorized",
};

export function error(message) {
  return { message };
}
