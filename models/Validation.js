export class Validation {
  #isOK;
  #errorMessage;

  constructor(isOK, errorMessage) {
    this.#isOK = isOK;
    this.#errorMessage = errorMessage;
  }

  isOK = () => this.#isOK;
  errorMessage = () => this.#errorMessage;
}
