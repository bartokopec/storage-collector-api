# storage-collector-api

API for Storage Collector mobile app.

## Configuration with `.env` file

This file is required both for development and for production.

```
CERT=""
CERT_KEY=""
CERT_PASS=""
PORT=0
API_KEY=""
DB_SERVICE=""
DB_TEST_QUERY=""
DB_SQL_STORAGE=""
DB_SQL_STORAGE_ENDDOC=""
DB_SQL_DOCINFO=""
DB_SQL_DOCS_LIST=""
DB_SQL_DOCDETAILS=""
DB_CONFIG=""
```

`DB_SERVICE` have to be: "firebird" or "mssql".

`DB_CONFIG` could to be:

- For Firebird: `"{"host":"string", "port":0, "database":"PATH", "user":"", "password":""}"`
- For MS SQL: `{"server":"string","options":{"database":"string"},"authentication":{"type":"string","options":{"userName":"string","password":"string"}}}` - more on https://github.com/tediousjs/node-mssql

`DB_SQL_STORAGE_ENDDOC` is optional. If filled, send of end-document will be - while inserting via `/api/collect`, additional fake item will be created with filled `Barcode` (`productCode`) field with value of this env variable.

## Basic dev run

0. Dont forget about installing dependecies with: `npm install` or `pnpm install`
1. Make sure, there's `.env` file, with configuration like described above.
2. `npm run start` or `pnpm start`

## Building for production

0. The same as above
1. If you want customize building, take a look to file `webpack.config.cjs` 
2. Build app with `npm run build` or `pnpm build` -> it will create new folder `dist` with just one file
3. Execute this app file with `node dist/<project name>.prod.cjs` (if it will have this name in webpack config)
5. Running this one-file app must be with `.env` file as well

## Endpoints

### GET Check

`/api/check`

Headers:

- `Authorization: API_KEY`

**Success return:**

Status: 200

**Error return:**

Status: 401

### GET Documents list

`/api/docs`

Headers:

- `Authorization: API_KEY`

Query:

- `login`

**Success return:**

Status: 200

Body:

```
[
  {
    "date": "datetime",
    "document": "string",
    "zone": "string",
    "quantity": 0,
    "description": "string"
  }
]
```

**Error return:**

Status: 401

Status: 400 if 'login' query parameter would be missing

### GET Document info

`/api/docinfo`

Headers:

- `Authorization: API_KEY`

Query:

- `id`: login
- `doc`: document number

**Success return:**

Status: 200

Body:

```
{
  "document": [
    {
      "Barcode": "string",
      "Description": "string",
      "Quantity": 1
    }
  ]
}
```

**Error return:**

Status: 401

Status: 400 if 'id' or 'doc' query parameter would missing

### POST Send collected data

`/api/collect`

Headers:

- `Authorization: API_KEY`

Body:

```
{
    "items": [
        {
            "userCode": "string",
            "docNumber": "string",
            "placeCode": "string",
            "productCode": "string",
            "count": 0
        }
    ]
}
```

**Success return:**

Status: 201

Body:

```
{
    "ids": [ "" ]
}
```

Array of "ids" contains strings.

**Error return:**

```
{
    "error": "message"
}
```
