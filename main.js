import "dotenv/config";
import { buildWebServer, secureAndLaunch } from "./serverBuilder.js";
import {
  saveCollected,
  getDocument,
  getDocsList,
} from "./endpoints/storage.js";
import { getDocDetails } from "./endpoints/docdetails.js";
import { saveCollectedValidation } from "./libs/validations/saveCollectedValidation.js";
import { getDocInfoValidation } from "./libs/validations/getDocInfoValidation.js";
import { getDocsValidation } from "./libs/validations/getDocsValidation.js";
import { getDocDetailsValidation } from "./libs/validations/getDocDetailsValidation.js";
import { healthCheckEnv } from "./libs/envs.js";
import {
  authorization,
  errorHandler,
  requestValidator,
} from "./libs/middlewares.js";
import { healthCheckDatabase } from "./libs/database.js";

healthCheckEnv();
await healthCheckDatabase();

const app = buildWebServer();

app.get("/api/check", authorization, (_, res) => res.status(200).send());
app.post(
  "/api/collect",
  authorization,
  (req, res, next) => requestValidator(saveCollectedValidation, req, res, next),
  saveCollected,
);
app.get(
  "/api/docinfo",
  authorization,
  (req, res, next) => requestValidator(getDocInfoValidation, req, res, next),
  getDocument,
);
app.get(
  "/api/docs",
  authorization,
  (req, res, next) => requestValidator(getDocsValidation, req, res, next),
  getDocsList,
);
app.get(
  "/api/docdetails",
  authorization,
  (req, res, next) => requestValidator(getDocDetailsValidation, req, res, next),
  getDocDetails,
);

// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => errorHandler(error, res));

secureAndLaunch(app);
