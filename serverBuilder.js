import https from "https";
import fs from "fs";
import express from "express";
import helmet from "helmet";
import cors from "cors";
import { env } from "./libs/envs.js";
// eslint-disable-next
import appInfo from "./package.mjs";

/** @returns {Express} */
export function buildWebServer() {
  const app = express();

  app.use(helmet());
  app.use(
    cors({
      origin: "*",
    }),
  );
  app.disable("x-powered-by");
  app.use(express.json());

  return app;
}

function getHttpsOptions() {
  const certFilePath = process.env.CERT_FILE;
  const certKeyPath = process.env.CERT_KEY_FILE;
  const passphrase = process.env.CERT_PASSWORD;
  const cert = fs.readFileSync(certFilePath);

  const options = {
    passphrase,
  };
  if (certFilePath.endsWith(".pfx")) {
    options.pfx = cert;
  } else {
    options.cert = cert;
    options.key = certKeyPath ? fs.readFileSync(certKeyPath) : "";
  }
  return options;
}

/** @param {Express} app  */
export function secureAndLaunch(app) {
  https.createServer(getHttpsOptions(), app).listen(env.PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`
        ${appInfo.name}
        version ${appInfo.version}
        with HTTPS, on port ${env.PORT}`);
  });
}
