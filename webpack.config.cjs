const path = require('path');
const appInfo = require('./package.json')

module.exports = {
  mode: 'production',
  entry: './main.js',
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: `${appInfo.name}.prod.${appInfo.version.replace('.','-')}.cjs`,
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.node$/,
        loader: "node-loader",
     }
    ]
  }
};