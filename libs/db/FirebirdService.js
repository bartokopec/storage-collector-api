import Firebird from "node-firebird";

export class FirebirdService {
  /** @type {Firebird.ConnectionPool} */
  #dbPool;

  /**
   * @param {Firebird.Options} options
   */
  constructor(options) {
    this.#dbPool = Firebird.pool(4, options);
  }

  /**
   * @param {string} sql
   * @param {any[]} paramsArray
   * @returns {Promise<any[]>}
   */
  executeQuery(sql, paramsArray) {
    return new Promise((resolve, reject) => {
      this.#dbPool.get((dbError, db) => {
        if (dbError) {
          reject(dbError);
          return;
        }
        db.query(sql, paramsArray, (queryError, result) => {
          if (queryError) {
            reject(queryError);
          } else {
            resolve(result);
          }
        });
      });
    });
  }
}
