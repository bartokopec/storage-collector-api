import mssql from "mssql";

export class MsSqlService {
  /** @type {mssql.config} */
  #options;

  constructor(options) {
    this.#options = options;
  }

  // TODO: ERRORS HANDLING
  /**
   * @param {string} sql
   * @param {Object} paramsDTO
   * @returns {Promise<any[]>}
   */
  async executeQuery(sql, paramsDTO) {
    const pool = await mssql.connect(this.#options);
    const request = pool.request();

    for (const key of Object.keys(paramsDTO)) {
      const param = paramsDTO[key];
      request.input(`${key}`, getSqlType(param), param);
    }

    const result = await request.query(sql);
    return result.recordset;
  }
}

// TODO: TESTS
function getSqlType(value) {
  switch (typeof value) {
    case "number":
      return mssql.Numeric;
    case "string":
      return mssql.VarChar;
    default:
      return mssql.Variant;
  }
}
