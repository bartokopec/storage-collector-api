import { InsertionData } from "../models/InsertionData.js";
import { AppError, Errors } from "../models/Errors.js";
import { SqlBadKeywords } from "../app.config.js";
import { env } from "./envs.js";
import { FirebirdService } from "./db/FirebirdService.js";
import { MsSqlService } from "./db/MsSqlService.js";

/** @type {FirebirdService|MsSqlService} */
const dbService = getService();

/**
 * @param {InsertionData} data
 * @returns {Promise<string>}
 */
export async function insertCollectedStorage(data) {
  if (!(data instanceof InsertionData)) {
    throw new AppError(Errors.BAD_FORMAT);
  }

  const dataArray = data.toDTO();
  const sql = env.DB_SQL_STORAGE;

  const result = await dbService.executeQuery(sql, dataArray);
  if (!result || result.length === 0) {
    throw new AppError("Insertion of collected item didn't result with ID");
  }

  return result[0].ID;
}

/**
 * @param {string} login
 */
export async function getDocuments(login) {
  const sql = env.DB_SQL_DOCS_LIST;

  const result = await dbService.executeQuery(sql, {
    Login: login,
  });

  return result;
}

/**
 * @param {string} docNumber
 * @param {string} id
 */
export async function getDocumentInfo(docNumber, id) {
  const sql = env.DB_SQL_DOCINFO;

  const result = await dbService.executeQuery(sql, {
    Document: docNumber,
    Login: id,
  });

  return result;
}

/**
 * @param {string} docNumber
 * @returns {Promise<string[]>}
 */
export async function getDocumentDetails(docNumber) {
  const sql = env.DB_SQL_DOCDETAILS;

  const result = await dbService.executeQuery(sql, {
    Document: docNumber,
  });

  return result.map((record) => record.Description);
}

export async function healthCheckDatabase() {
  // eslint-disable-next-line no-console
  console.log("Database checking ...");

  const testSql = env.DB_TEST_QUERY;

  validateSql(testSql);
  validateSql(env.DB_SQL_STORAGE);
  validateSql(env.DB_SQL_DOCINFO);
  validateSql(env.DB_SQL_DOCS_LIST);
  validateSql(env.DB_SQL_DOCDETAILS);

  // eslint-disable-next-line no-console
  console.log("Executing query for test ...");
  await dbService.executeQuery(testSql, []);
  // eslint-disable-next-line no-console
  console.log("OK: Database");
}

/** @param {string} sql */
function validateSql(sql) {
  SqlBadKeywords.forEach((keyword) => {
    if (sql.toLowerCase().includes(` ${keyword.toLowerCase()} `)) {
      throw new Error(
        `[APP] validateSql: the "${sql}" has potentially dangerous SQL code.\n` +
          `Found in array of keywords: ${SqlBadKeywords}`,
      );
    }
  });
  // eslint-disable-next-line no-console
  console.log(`OK: test words in '${sql}'`);
}

/** @returns {FirebirdService|MsSqlService2}*/
function getService() {
  const dbOptions = env.DB_CONFIG;
  switch (env.DB_SERVICE) {
    case "firebird":
      return new FirebirdService(dbOptions);
    case "mssql":
      return new MsSqlService(dbOptions);
    default:
      throw new Error("There's no such service connected with DB_SERVICE key");
  }
}
