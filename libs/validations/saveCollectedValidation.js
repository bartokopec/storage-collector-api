import { Validation } from "../../models/Validation.js";
import { checkSchema } from "../schemaValidator.js";
import { InsertionDataSchema } from "../../models/InsertionData.js";

/**
 * @param {Request} req
 * @returns {Validation}
 */
export function saveCollectedValidation(req) {
  if (!req.body.items) {
    return new Validation(false, "'items' array field is missing!");
  }
  for (const item of req.body.items) {
    if (!checkSchema(InsertionDataSchema, item)) {
      return new Validation(false, "One of items dont match requirements!");
    }
  }

  return new Validation(true);
}
