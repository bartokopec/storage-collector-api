import { Validation } from "../../models/Validation.js";

/**
 * @param {Request} req
 * @returns {Validation}
 */
export function getDocDetailsValidation(req) {
  const { doc } = req.query;
  if (!doc) {
    return new Validation(false, "'doc' query parameter is missing!");
  }

  return new Validation(true);
}
