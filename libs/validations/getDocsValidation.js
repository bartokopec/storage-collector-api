import { Validation } from "../../models/Validation.js";

/**
 * @param {Request} req
 * @returns {Validation}
 */
export function getDocsValidation(req) {
  const { login } = req.query;

  if (!login) {
    return new Validation(false, "'login' query parameter is missing!");
  }

  return new Validation(true);
}
