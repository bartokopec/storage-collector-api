import { Validation } from "../../models/Validation.js";

/**
 * @param {Request} req
 * @returns {Validation}
 */
export function getDocInfoValidation(req) {
  const { id, doc } = req.query;
  if (!id || !doc) {
    return new Validation(false, "'id' or 'doc' query parameter is missing!");
  }

  return new Validation(true);
}
