import Ajv from "ajv";

const ajv = new Ajv();

export function checkSchema(schema, data) {
  const validate = ajv.compile(schema);
  return validate(data);
}
