import fs from "fs";
import dayjs from "dayjs";
import { Errors, error } from "../models/Errors.js";
import { env } from "./envs.js";

/**
 * @callback Validator
 * @param {Request} req
 * @returns {import('../models/Validation.js').Validation}
 */
/**
 * @param {Validator} callback
 * @param {Request} req
 * @param {Response} res
 */
export function requestValidator(callback, req, res, next) {
  const validation = callback(req);
  if (!validation.isOK()) {
    res.status(400).send(error(validation.errorMessage()));
    return;
  }
  next();
}

/**
 * @param {Request} req
 * @param {Response} res
 */
export function authorization(req, res, next) {
  if (req.headers["authorization"] !== env.API_KEY) {
    res.status(401).send(error(Errors.UNAUTHORIZED));
  } else {
    next();
  }
}

/**
 * @param {Error} err
 * @param {Response} res
 */
export function errorHandler(err, res) {
  if (err) {
    if (res.statusCode < 400) {
      res.status(500);
    }
    if (res.statusCode >= 500) {
      // eslint-disable-next-line no-console
      console.error("[ERROR]", err.message);
      fs.appendFile(
        ".log",
        `${dayjs().format("YYYY-MM-DD_HH:mm")} ${err.stack}\n\n`,
        () => {},
      );
      err.message = "API internal error";
    }
    res.send({ error: err.message });
  }
}
