import { AppError } from "../models/Errors.js";

export function healthCheckEnv() {
  const check =
    !!process.env.PORT &&
    !!process.env.API_KEY &&
    !!process.env.DB_SERVICE &&
    !!process.env.DB_SQL_STORAGE &&
    !!process.env.DB_SQL_DOCS_LIST &&
    !!process.env.DB_SQL_DOCINFO &&
    !!process.env.DB_TEST_QUERY &&
    !!process.env.DB_CONFIG;
  if (!check) {
    throw new AppError(
      "[APP] healthCheckEnv: list of environments did not meet the requirements. Check the docs of API.",
    );
  }
  // eslint-disable-next-line no-console
  console.log("OK: environments variables (.env file)");
}

export const env = {
  PORT: parseInt(process.env.PORT),
  DB_SERVICE: process.env.DB_SERVICE,
  DB_SQL_STORAGE: process.env.DB_SQL_STORAGE,
  DB_SQL_STORAGE_ENDDOC: process.env.DB_SQL_STORAGE_ENDDOC,
  DB_SQL_DOCS_LIST: process.env.DB_SQL_DOCS_LIST,
  DB_SQL_DOCINFO: process.env.DB_SQL_DOCINFO,
  DB_SQL_DOCDETAILS: process.env.DB_SQL_DOCDETAILS,
  DB_CONFIG: JSON.parse(process.env.DB_CONFIG),
  DB_TEST_QUERY: process.env.DB_TEST_QUERY,
  API_KEY: process.env.API_KEY,
};
