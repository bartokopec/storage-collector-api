import { getDocumentDetails } from "../libs/database.js";

/**
 * @param {Request} req
 * @param {Response} res
 * @param {import("express").NextFunction} next
 */
export async function getDocDetails(req, res, next) {
  try {
    const { doc } = req.query;
    const result = await getDocumentDetails(doc);

    res.status(200);
    res.send(result);
  } catch (err) {
    res.status(500);
    next(err);
  }
}
