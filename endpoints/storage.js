import {
  insertCollectedStorage,
  getDocumentInfo,
  getDocuments,
} from "../libs/database.js";
import { InsertionData } from "../models/InsertionData.js";
import { env } from "../libs/envs.js";

/**
 * @param {Request} req
 * @param {Response} res
 * @param {import("express").NextFunction} next
 */
export async function getDocsList(req, res, next) {
  try {
    const { login } = req.query;
    const result = await getDocuments(login);

    res.status(200);
    res.send(
      result.map((document) => ({
        date: document["Date"],
        document: document["Document"],
        zone: document["Zone"],
        quantity: document["Quantity"],
        description: document["Description"],
      })),
    );
  } catch (err) {
    res.status(500);
    next(err);
  }
}

/**
 * @param {Request} req
 * @param {Response} res
 * @param {import("express").NextFunction} next
 */
export async function getDocument(req, res, next) {
  try {
    const { id, doc } = req.query;
    const result = await getDocumentInfo(doc, id);

    res.status(200);
    res.send({
      document: result.map((document) => ({
        barcode: document["Barcode"],
        quantity: document["Quantity"],
        description: document["Description"],
      })),
    });
  } catch (err) {
    res.status(500);
    next(err);
  }
}

/**
 * @param {Request} req
 * @param {Response} res
 * @param {import("express").NextFunction} next
 */
export async function saveCollected(req, res, next) {
  try {
    const insertionIds = [];
    for (const insertion of req.body.items) {
      const id = await insertCollectedStorage(
        new InsertionData(
          insertion.userCode,
          insertion.docNumber,
          insertion.placeCode,
          insertion.productCode,
          insertion.count,
        ),
      );
      insertionIds.push(id);
    }
    const last = req.body.items[req.body.items.length - 1];
    if (env.DB_SQL_STORAGE_ENDDOC) {
      await insertCollectedStorage(
        new InsertionData(
          last.userCode,
          last.docNumber,
          "",
          env.DB_SQL_STORAGE_ENDDOC,
          0,
        ),
      );
    }

    res.status(201);
    res.send({ ids: insertionIds });
  } catch (err) {
    res.status(500);
    next(err);
  }
}
