export const SqlBadKeywords = [
  "delete",
  "create",
  "drop",
  "backup",
  "table",
  "database",
  "alter",
  "view",
];
